# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyApp::Application.config.secret_key_base = '9f0a56510ab641e6481495b867bea1fc212a07a073aa7bb2f399b8b885021d1f472d4909603c2efb354199ccefcb71d003a62c1be01cad5b5600fd70e4613c8f'
